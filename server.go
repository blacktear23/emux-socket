package main

import (
	"errors"
	"log"
	"net"
	"sync/atomic"
	"time"

	"bitbucket.org/blacktear23/emux"
)

var (
	ErrUnknownMode = errors.New("Unknown Mode")
)

type Server struct {
	cfg        *Config
	emuxCfg    *emux.Config
	mode       string
	id         uint64
	size       uint64
	dialerList []*net.Dialer
}

func NewServer(cfg *Config) (*Server, error) {
	mode := cfg.Service.Mode
	if mode != "unix" && mode != "tcp" {
		return nil, ErrUnknownMode
	}
	var size uint64
	if cfg.Service.LocalIPList != nil {
		size = uint64(len(cfg.Service.LocalIPList))
	}
	dialerList := make([]*net.Dialer, size)
	timeout := 30
	if cfg.Service.Timeout > 0 {
		timeout = cfg.Service.Timeout
	}
	for i, addr := range cfg.Service.LocalIPList {
		laddr, err := net.ResolveIPAddr("ip", addr)
		if err != nil {
			return nil, err
		}
		taddr := &net.TCPAddr{
			IP: laddr.IP,
		}
		dialer := &net.Dialer{
			Timeout:   time.Duration(timeout) * time.Second,
			LocalAddr: taddr,
		}
		dialerList[i] = dialer
	}

	return &Server{
		cfg:        cfg,
		emuxCfg:    cfg.GetEmuxConfig(),
		mode:       cfg.Service.Mode,
		size:       size,
		dialerList: dialerList,
	}, nil
}

func (s *Server) dialUnix() (net.Conn, error) {
	return net.Dial("unix", s.cfg.Service.TargetSocketFile)
}

func (s *Server) dialTCP() (net.Conn, error) {
	nid := atomic.AddUint64(&s.id, 1)
	id := int(nid % s.size)
	dialer := s.dialerList[id]
	return dialer.Dial("tcp", s.cfg.Service.TargetAddress)
}

func (s *Server) dialTarget() (net.Conn, error) {
	switch s.mode {
	case "unix":
		return s.dialUnix()
	case "tcp":
		return s.dialTCP()
	}
	return nil, ErrUnknownMode
}

func (s *Server) processStream(conn net.Conn) {
	remote, err := s.dialTarget()
	if err != nil {
		log.Printf("Dial to %s error: %v", s.cfg.Service.TargetSocketFile, err)
		conn.Close()
		return
	}
	ProxyPipe(conn, remote)
}

func (s *Server) processConn(conn net.Conn) {
	// At here we add panic recover to prevent system exit.
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
			conn.Close()
		}
	}()

	session, err := emux.Server(conn, s.emuxCfg)
	if err != nil {
		log.Println(err)
		return
	}
	for {
		stream, err := session.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go s.processStream(stream)
	}
}

func (s *Server) Run() {
	listener, err := net.Listen("tcp", s.cfg.ServerListen)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen on:", s.cfg.ServerListen)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go s.processConn(conn)
	}
}
