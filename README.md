# emux-socket

emux-socket is unix socket proxy. emux-socket is based on emux.

## Basic Usage

```
           +-------+                   +------------------+
Client --> | Nginx |<-- unix-socket -->| emux-socket -m c |
           +-------+                   +------------------+
			                                     |
			                                 TCP + emux
			                                     |
           +-------+                   +------------------+
           | Nginx |<-- unix-socket -->| emux-socket -m s |
           +-------+                   +------------------+
```

## Command Line Help

```
Usage of ./emux-socket:
  -P	Enable profile, it will create emux-socket.prof file.
  -c string
    	Config file
  -m string
    	Mode (s|c), s: server, c: client (default "s")
  -v	Print version
```

## config.json

```json
{
	"ServerListen": "192.168.1.5:9000",
	"Cipher": "",
	"Password": "",
	"PoolSize": 100,
	"Service": {
		"Mode": "tcp",
		"ClientSocketFile": "/tmp/local.sock",
		"TargetSocketFile": "/tmp/nginx",
		"TargetAddress": "192.168.1.5:80",
		"LocalIPList": ["192.168.1.10", "192.168.1.11"],
		"Timeout": 30
	}
}
```

* ServerListen: Server mode listen address, Client mode mean server's address and TCP port
* Cipher: Encryption setting, empty string means no encrypt data.
	* empty string
	* aes-128-gcm
	* aes-192-gcm
	* aes-256-gcm
* Password: Encrypt password
* PoolSize: emux pool size, if not set default is 100
* Service:
 	* Mode: Server create connection mode: value should be `tcp` or `unix`
	* ClientSocketFile: Client mode unix-socket file path
	* TargetSocketFile: Server mode target unix-socket file path, used by Mode = unix
	* TargetAddress: Server mode target TCP address, used by Mode = tcp
	* LocalIPList: Local IP list used to create connection to target, used by Mode = tcp
	* Timeout: Connection timeout, if not set default is 30 second, used by Mode = tcp

## Multi Local IP Usage

On server side, you can configure multi local IP address to create more connection. The configuration should like this:

You have a private NIC (eth1) and configure it's IP to `192.168.1.5` and your application listen to this address and port is `8080`.

Then use ifconfig to bind N (such as 3) IP address to eth1:

```
# ifconfig eth1:1 192.168.1.50/24 up
# ifconfig eth1:2 192.168.1.51/24 up
# ifconfig eth1:3 192.168.1.52/24 up
```

Then modify `config.json` and use tcp mode:

```json
{
    "ServerListen": "192.168.1.5:9000",
    "PoolSize": 100,
    "Service": {
        "Mode": "tcp",
        "ClientSocketFile": "/tmp/local.sock",
        "TargetAddress": "192.168.1.5:8080",
        "LocalIPList": ["192.168.1.50", "192.168.1.51", "192.168.1.52"]
    }
}
```

And start emux-socket to use this config file

```
# ./emux-socket -c ./config.json -m s
```

Then your server can serve more than 90k connections and not facing TCP port exhausted.