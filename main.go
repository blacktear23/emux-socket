package main

import (
	"flag"
	"log"
	"os"
)

const VERSION = "1.0.1-beta"

func main() {
	log.SetOutput(os.Stdout)
	var config, mode string
	var profile, ver bool
	flag.BoolVar(&profile, "P", false, "Enable profile, it will create emux-socket.prof file.")
	flag.BoolVar(&ver, "v", false, "Print version")
	flag.StringVar(&mode, "m", "s", "Mode (s|c), s: server, c: client")
	flag.StringVar(&config, "c", "", "Config file")
	flag.Parse()

	if ver {
		log.Println("emux-socket Version:", VERSION)
		os.Exit(0)
	}

	if profile {
		EnableProfile("emux-socket.prof")
	}

	CheckFile(config)
	cfg, err := NewConfig(config)
	if err != nil {
		flag.PrintDefaults()
		log.Fatal(err)
	}
	var c *Client
	var s *Server
	if mode == "s" {
		log.Println("Start Server Mode")
		s, err = NewServer(cfg)
		if err != nil {
			log.Fatal(err)
		}
		go s.Run()
	} else if mode == "c" {
		log.Println("Start Client Mode")
		c, err = NewClient(cfg)
		if err != nil {
			log.Fatal(err)
		}
		go c.Run()
	} else {
		log.Fatal("Unknown Mode")
	}
	WaitSignal(profile, func() {
		if mode == "c" {
			c.Shutdown()
			sockFile := cfg.Service.ClientSocketFile
			os.Remove(sockFile)
		}
	}, nil)
}
