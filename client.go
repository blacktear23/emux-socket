package main

import (
	"log"
	"net"
	"os"
	"sync"

	"bitbucket.org/blacktear23/emux"
)

var DefaultSize = 100

type Client struct {
	cfg      *Config
	emuxCfg  *emux.Config
	pool     *emux.Pool
	lock     sync.Mutex
	listener net.Listener
	size     int
}

func NewClient(cfg *Config) (*Client, error) {
	size := DefaultSize
	if cfg.PoolSize != 0 {
		size = cfg.PoolSize
	}
	emuxCfg := cfg.GetEmuxConfig()
	pool, err := emux.NewPool(cfg.ServerListen, emuxCfg, size)
	if err != nil {
		return nil, err
	}
	return &Client{
		cfg:     cfg,
		emuxCfg: emuxCfg,
		pool:    pool,
		size:    size,
	}, nil
}

func (c *Client) dialViaServer() (net.Conn, error) {
	if c.pool == nil {
		pool, err := emux.NewPool(c.cfg.ServerListen, c.emuxCfg, c.size)
		if err != nil {
			return nil, err
		}
		c.lock.Lock()
		if c.pool == nil {
			c.pool = pool
		} else {
			pool.Close()
		}
		c.lock.Unlock()
	}
	return c.pool.Open()
}

func (c *Client) processConn(conn net.Conn) {
	// At here we add panic recover to prevent system exit.
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
			conn.Close()
		}
	}()

	remote, err := c.dialViaServer()
	if err != nil {
		log.Println("Connect to server fail:", err)
		return
	}
	ProxyPipe(conn, remote)
}

func (c *Client) Run() {
	listener, err := net.Listen("unix", c.cfg.Service.ClientSocketFile)
	if err != nil {
		log.Fatal(err)
	}
	os.Chmod(c.cfg.Service.ClientSocketFile, 0777)
	c.listener = listener
	log.Println("Listen on:", c.cfg.Service.ClientSocketFile, "Remote server:", c.cfg.Service.TargetSocketFile)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			break
		}
		go c.processConn(conn)
	}
}

func (c *Client) Shutdown() {
	c.listener.Close()
}
