CURDIR      := $(shell pwd)
LDFLAGS     := -s -w
GOBUILD     := GOARCH=amd64 CGO_ENABLED=0 go build -ldflags "$(LDFLAGS)"
BUILDPATH   := $(CURDIR)/build

.PHONY: all linux darwin

all: linux darwin

linux:
	@mkdir -p $(BUILDPATH)/linux-amd64
	GOOS=linux $(GOBUILD) -o $(BUILDPATH)/linux-amd64/emux-socket

darwin:
	@mkdir -p $(BUILDPATH)/darwin-amd64
	GOOS=darwin $(GOBUILD) -o $(BUILDPATH)/darwin-amd64/emux-socket
