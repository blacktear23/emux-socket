package main

import (
	"encoding/json"
	"os"

	"bitbucket.org/blacktear23/emux"
)

type Service struct {
	ClientSocketFile string   `json:"ClientSocketFile"`
	Mode             string   `json:"Mode"`
	TargetSocketFile string   `json:"TargetSocketFile"`
	TargetAddress    string   `json:"TargetAddress"`
	LocalIPList      []string `json:"LocalIPList"`
	Timeout          int      `json:Timeout`
}

type Config struct {
	ServerListen string  `json:"ServerListen"`
	Cipher       string  `json:"Cipher"`
	Password     string  `json:"Password"`
	PoolSize     int     `json:"PoolSize"`
	Service      Service `json:"Service"`
	configFile   string
	emuxCfg      *emux.Config
}

func NewConfig(cfgFile string) (*Config, error) {
	ret := &Config{
		configFile: cfgFile,
		emuxCfg:    nil,
	}
	err := ret.Reload()
	return ret, err
}

func (c *Config) Reload() error {
	fp, err := os.Open(c.configFile)
	if err != nil {
		return err
	}
	defer fp.Close()
	return json.NewDecoder(fp).Decode(c)
}

func (c *Config) GetEmuxConfig() *emux.Config {
	if c.emuxCfg == nil {
		c.emuxCfg = emux.DefaultConfig()
		c.emuxCfg.CipherType = c.Cipher
		c.emuxCfg.Password = c.Password
	}
	return c.emuxCfg
}
